import {Formik, Form} from 'formik';
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {validationSchema} from "./validation.js"

import {selectFromData} from "../../store/selectors.js";
import {actionUpdateCv} from "../../store/actions.js";

import InputBox from './Input/Input.jsx';
import Button from '../Button/Button.jsx';
import './Formset.scss'



const Formset = () => {
    const formData = useSelector(selectFromData)
	//const navigate = useNavigate()
	const dispatch = useDispatch()



    return (
      <Formik
      initialValues={formData}
      validationSchema={validationSchema}
      onSubmit={(values, {resetForm}) => {
          console.log('values', values);
          console.log(resetForm);
          dispatch(actionUpdateCv(values))
          //navigate('/preview')
          resetForm()
          localStorage.clear('basket')
          window.location.reload();
      }}

     >
       {({errors, touched}) => {
         
            console.log('errors', errors);
            console.log("touched", touched)
            return (
                <Form>
                    <fieldset className="form-block">
                        <legend >Оформити замовлення</legend>
                        
                                <InputBox
                                    className="form-item"
                                    label="Ім’я"
                                    name="name"
                                    placeholder=""
                                    error={errors.name && touched.name}
                                />
                                <InputBox
                                    className="form-item"
                                    label="Прізвище"
                                    name="surname"
                                    placeholder=""
                                    error={errors.surname && touched.surname}
                                />
                                <InputBox
                                    className="form-item"
                                    label="Вік"
                                    name="age"
                                    placeholder=""
                                    error={errors.age && touched.age}
                                />
                                <InputBox
                                    className="form-item"
                                    name="adress"
                                    label="Адреса"
                                    placeholder=''
                                    error={errors.adress && touched.adress}
                                />
                                <InputBox
                                    className="form-item"
                                    name="email"
                                    label="Адреса"
                                    placeholder=''
                                    error={errors.email && touched.email}
                                />
                                <InputBox
                                    className="form-item"
                                    name="phone"
                                    label="Телефон"
                                    placeholder='+38'
                                    error={errors.phone && touched.phone}
                                />   
                                <div className="form-item">
                                  <Button underlineView type="submit">Купити</Button>
                                </div>  
                    </fieldset>    
                    
                </Form>
            )
        }}

       
     </Formik>
    )
}

export default Formset