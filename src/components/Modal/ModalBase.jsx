import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import "./Modal.scss"


const ModalBase = ({handleOk, handleClose, isOpen}) =>{

    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
            handleClose()
        }
    }


    return(
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                   <ModalClose click={handleClose}/>
                </ModalHeader>
                <ModalBody>
                    <img className='modal-image'></img>
                    <h4>Видалити товар</h4>
                    <p></p>
                </ModalBody>
                <ModalFooter textFirst="скасувати" textSecondary="видалити" clickFirst={handleClose} clickSecondary={() => {
                    handleOk()
                    handleClose()
                }}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    desk: PropTypes.string,
    handleOk: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}



export default ModalBase
