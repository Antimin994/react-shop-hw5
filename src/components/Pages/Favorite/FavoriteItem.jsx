import Button from "../../Button/Button";
import {useState} from "react";
import cx from "classnames";
import "./Favorite.scss";
import { useDispatch, useSelector } from "react-redux";

const FavoriteItem = ({favorite})=>{
    const dispatch = useDispatch();
    const {good_img, title, original_title, price, id, original_name, name} = favorite
    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`
    const handleFavorite = () => {
        const favoriteAction = (data) => {
          return {
            type: 'HANDLE_FAVORITE',
            payload: favorite,
          };
        };
        dispatch(favoriteAction('Some payload data'));
      };
    
    return (
        <>
           <div className="favorite-desc">
                      <img src={good_img} height={'376px'} alt={title ? title : name}/>
                        <div className='favorite-desc-back'>
                            <h3 className="favorite-desc__title">{title ? title : name}</h3>
                            <p className="favorite-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                            <p className="favorite-desc__price">Ціна {price} грн</p>
                            <div className="button__wrapper">
                                <Button boxView >до кошика</Button>
                                <Button underlineView onClick={()=>dispatch(handleFavorite(favorite)) }>видалити</Button>
                            </div>
                        </div>
            </div>

        </>
        
    )
}


export default FavoriteItem
