import Button from "../../Button/Button";
import {useState} from "react";
import cx from "classnames";
import "./Basket.scss"
import { useDispatch, useSelector } from "react-redux";

const BasketItem = ({basket, handleModal})=>{
    let {good_img, title, original_title, price, id, original_name, name, quantity} = basket
    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`
    const dispatch = useDispatch();

    const plusItem = () => {
        const plusAction = (data) => {
          return {
            type: 'PLUS_BASKET',
            payload: basket,
          };
        };
        dispatch(plusAction('Some payload data'));
    };
    const minusItem = () => {
        const minusAction = (data) => {
          return {
            type: 'MINUS_BASKET',
            payload: basket,
          };
        };
        dispatch(minusAction('Some payload data'));
      };
      const removeBasket = () => {
        const removeAction = (data) => {
          return {
            type: 'REMOVE_BASKET',
            payload: basket,
          };
        };
        dispatch(removeAction('Some payload data'));
      };

    
    return (
        <>
           <div className="basket-desc">
                      <img src={good_img} height={'376px'} alt={title ? title : name}/>
                        <div className='basket-desc-back'>
                            <h3 className="basket-desc__title">{title ? title : name}</h3>
                            <div className="button__counter">
                               <Button boxView onClick={()=>dispatch(minusItem(basket))} className="btn_counter">-</Button>
                               <span className="text_counter">{quantity === undefined ? 1 : quantity}</span>
                               <Button onClick={()=>dispatch(plusItem(basket))} boxView className="btn_counter">+</Button>
                            </div>
                            <p className="basket-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                            <p className="basket-desc__price">Ціна {price} грн</p>
                            <div className="button__wrapper">
                                <Button boxView >купити</Button>
                                <Button underlineView onClick={ 
                                    ()=>{handleModal()
                                        dispatch(removeBasket(basket))
                                    }
                                    }>видалити</Button>
                            </div>
                        </div>
            </div>

        </>
        
    )
}


export default BasketItem
