import {createAction} from "@reduxjs/toolkit";
import {sendRequest} from '../helpers/sendRequest.js'

export const handleFavorite = createAction("HANDLE_FAVORITE")
export const handleBasket = createAction("HANDLE_BASKET")
export const handleConfirmed = createAction("HANDLE_CONFIRMED")
export const handleModal = createAction("HANDLE_MODAL")
export const handleModalImage = createAction("HANDLE_MODAL_IMAGE")
export const plusBasket = createAction("PLUS_BASKET")
export const minusBasket = createAction("MINUS_BASKET")
export const removeBasket = createAction("REMOVE_BASKET")
export const actionAddToClothes = createAction("ACTION_ADD_CLOTHES")
export const actionAddToAccessories = createAction("ACTION_ADD_ACCESSORIES")
export const actionUpdateCv = createAction("ACTION_UPDATE_CV")

export const actionFetchClothes = () => (dispatch) => {

	return sendRequest(`../../public/clothes.json`)
		.then((clothes) => {
			dispatch(actionAddToClothes(clothes))
		})
}
export const actionFetchAccessories = () => (dispatch) => {
	return sendRequest(`../../public/accessories.json`)
		.then((accessories) => {
			dispatch(actionAddToAccessories(accessories))
		})
}
